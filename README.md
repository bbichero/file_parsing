# File parsing
Bash script to parse a particulars configurations files

### For a full use 3 array must be define

### keywords for stock keyword variable that will contain string only 
``keywords=("key-1" "key-2")``

### keywords_array for stock keyword variable that will contain multiple string
``keywords_array=("key-3" "key-4" "key-5")``

### keywords_mand for stock mandatory keyword variable, if an other keyword is find, script stop 
``keywords_mand=("key-1" "key-4")``

### check_next_line()
To use this function you must to declare a keywords array
with all keywords you need to have in your argument file

### ft_valid_keyword()
Take one parameter and loop on keywords_array and keywords variable to find the given keyword
If at least one of the 2 array don't containt it, script exit

### check_value()
Take one parameter and check his format
Exit if the grep return nothing

### check_array()
Take one parameter and check his format
Exit if the grep return nothing
