# Take a valid keywords and an array in arg and return
# the valud corresponding from clear file
function	ft_get_value()
{
	# Can't debug here before all echo is used for return

	# Redeclare array from 2nd argument
	local clean_array=("${@:2}")
	local return_array
	local j=0

	for i in "${!clean_array[@]}"
	do
		# Split line with ':' delimitor
		IFS=':' read -r -a split_line <<< "${clean_array[$i]}"

		# Trim the 2 new line
		split_line[0]=$(echo "${split_line[0]}" | xargs)
		split_line[1]=$(echo "${split_line[1]}" | xargs | sed 's/ //g')

		# Check if split_line match with 2nd argument given
		if [ "${split_line[0]}" == "$1" ]
		then
			# Add to return_array the value find, adn continu looping
			return_array[$j]="${split_line[1]}"

			j=$j+1
		fi
		unset split_line
	done

	# Print whole array
	echo "${return_array[@]}"

	# Unset variable
	unset clean_array return_array

	return 1
}
