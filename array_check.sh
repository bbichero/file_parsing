# Check if argument is valid keyword
function	ft_valid_keyword()
{
	# If debug>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in ft_valid_keyword()"${NC}
	fi
	# We loop on keyword array
	for i in "${!keywords[@]}"
	do
		# If argument if egal to a value in array return 0
		if [ "${keywords[$i]}" == "$1" ]
		then
			return 0
		fi
	done

	# We loop on keyword_mand array
	for i in "${!keywords_array[@]}"
	do
		# If argument if egal to a value in array return 0
		if [ "${keywords_array[$i]}" == "$1" ]
		then
			return 0
		fi
	done

	# Unset variables
	unset i

	# If scirpt goes here, first argument is invalid
	echo -e ${RED}"Invalid keyword: '`echo $1`', abort"${NC}
	exit 1
}

# Check value format in parameter
# Value format must be like : "value1" or 'value1'
function	check_value()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in check_value()"${NC}
	fi

	local value_array=("$@")

	# Check value syntax, can be a simple value simple or double quoted
	# Or an array delimitd by '{}' and with simple or double quoted values
	# Check with regex, if it's simple value
	value_pattern="^[\'\"]?[^\s\'\"]*[\'\"]?$"
	# Trim value before apply regex
	trim_line=$(echo "${value_array[1]}" | xargs)
	value_match=$(echo "$trim_line" | grep -Po "$value_pattern")
	# If debug print matcha result
	if [ $DEBUG -gt 0 ]
	then
		echo "Regex pattern for match simple value: '`echo $value_pattern`'"
		echo "Value evaluated			 : '`echo $trim_line`'"
		echo -e ${GRE}"Return of grep regex command	 : '`echo $value_match`'"${NC}
	fi

	# If regex return nothing stop parsing
	if [ ${#value_match} -eq 0 ]
	then
		echo -e ${RED}"Grep regex command return nothing"
		echo -e "Check argument file, abort"${NC}
		echo -e ${RED}">>>>"${NC} "${value_array[0]}" ${RED}"<<<<"${NC}
		exit 1
	fi

	# Unset variable
	unset value_array value_pattern value_match
}

# Check array format in parameter
# Array format must be like: {'value1', 'value2'},
# or {"value3","value4"}, or {value5, value6}
function	check_array()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in check_array()"${NC}
	fi

	local value_array=("$@")

	# Check if value is an array
	array_pattern="{([\'\"]?[^\s\'\"]*[\'\"]?,?(, )?)+}"
	# Trim value before apply regex
	array_line=$(echo "${value_array[1]}" | xargs)
	array_match=$(echo "$array_line" | grep -Po "$array_pattern")

	# If debug print matcha result
	if [ $DEBUG -gt 0 ]
	then
		echo "Regex patern for match array value: '`echo $array_pattern`'"
		echo "Value evaluated			: '`echo $array_line`'"
		echo -e ${GRE}"Return of grep regex command	: '`echo $array_match`'"${NC}
	fi

	# If regex return nothing stop parsing
	if [ ${#array_match} -eq 0 ]
	then
		echo -e ${RED}"Grep regex command return nothing"
		echo -e "Check argument file, abort"${NC}
		echo -e ${RED}">>>>"${NC} "${value_array[0]}" ${RED}"<<<<"${NC}
		exit 1
	fi

	# Unset variable
	unset value_array array_line array_match
}

# Check line validity
function	check_next_line()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo -e ${YEL}"Enter in check_next_line()"${NC}
	fi

	# Check if line contain at least:
	# - A valid keywork followgin of ':'
	# - A value delimited by "'"or '"'
	# OR an array with values like :
	# {'value1'}, {"value2"}

	# Split line with ':' delimitor
	IFS=':' read -r -a split_line <<< "$1"

	# If debug =1 show what split done
	if [ $DEBUG -gt 0 ]
	then
		echo "Line before split: '`echo $1`'"
		echo -e ${YEL}"IFS=':' read -r -a split_line <<< $1"${NC}
		echo "Array created from split"
		echo '{'${split_line[@]}'}'
	fi

	# If split does not output 2 line, print error
	if [ ${#split_line[@]} -ne 2 ]
	then
		echo -e ${YEL}"Line '`echo $1`' is not well formated, abort"${NC}
		exit 1
	fi

	# Trim the 2 new line
	split_line[0]=$(echo "${split_line[0]}" | xargs)
	split_line[1]=$(echo "${split_line[1]}" | xargs)

	# Check if split_line[0] contain valie keyword
	# If it contain a basic keyword, we check the value
	# else if it contain array keyword, we check that array
	# Else print error and exit
	if ! ft_array_contains "${split_line[0]}" "${keywords[@]}"
	then
		check_value "${split_line[@]}"
	elif ! ft_array_contains "${split_line[0]}" "${keywords_array[@]}"
	then
		check_array "${split_line[@]}"
	else
		echo -e ${RED}"Error in line: '`echo $1`', keyword: '`echo ${split_line[0]}`' is incorrect"${NC}
		exit 1
	fi

	# Add clean line to file for use later
	echo "${split_line[0]} : ${split_line[1]}" >> ${new_file}

	# Remove keyword from keywords_mand array
	if ! ft_array_contains "${split_line[0]}" "${keywords_mand[@]}"
	then
		index_key=$(ft_get_index "${split_line[0]}" "${keywords_mand[@]}")
		unset keywords_mand[$index_key] index_key
		keywords_mand=("${keywords_mand[@]}")

		# If $DEBUG>1 print value unset
		if [ $DEBUG -gt 0 ]
		then
			echo -e ${GRE}"Mandatory keyword '`echo "${split_line[0]}"`' will be unset"${NC}
			echo -e ${YEL}"Following keyword are still here:"
			echo -e "'${keywords_mand[@]}'"${NC}
		fi
	fi

	# Unset variable
	unset split_line
}

